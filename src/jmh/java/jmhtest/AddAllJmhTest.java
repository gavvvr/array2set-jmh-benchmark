package jmhtest;

import org.openjdk.jmh.annotations.*;

import java.util.*;
import java.util.concurrent.TimeUnit;

@State(Scope.Thread)
public class AddAllJmhTest {

    private int maxSize = 10_000_000;
    private String[] s = new String[maxSize];
    private Random r = new Random();

    @Setup
    public void prepare() {
        for (int i = 0; i < maxSize; i++) {
            s[i] = "" + r.nextInt(100);
        }
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    public Set<String> usingConstructor() {
        return new HashSet<String>(Arrays.asList(s));
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    public Set<String> withIntermediateArray() {
        HashSet<String> strings = new HashSet<>(s.length);
        strings.addAll(Arrays.asList(s));
        return strings;
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    public Set<String> withoutIntermediateArray() {
        HashSet<String> strings = new HashSet<>(s.length);
        Collections.addAll(strings, s);
        return strings;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(new AddAllJmhTest().s));
    }
}
